**Privacy Policy**

Data Protection Privacy Policies
At KC MICROFINANCE, we treat your privacy with the highest importance. Our Data Protection Privacy Policy details the measures we take to preserving and safely guarding your privacy when you visit our website or communicate with our personnel. This policy has been approved and provided by our legal advisors.
These Policies Include:
1. Data Collection
2. Your Personal Data and how it is Used
3. Change of Purpose
4. Your Personal Data Rights
5. Persons who have access to your Personal Data
6. Security & Confidentiality
7. Transfer of Personal Data outside Nigeria
8. Retention of Personal Data
9. Third Party Links
10. Cookies Policy
11. Subject Access Request Response Procedure
We undertake regular updates to our Data Protection Privacy Policies which we will notify you of on our website and/or by email.
Glossary
Affiliated Third Parties includes companies with which we have common ownership or management or other contractual strategic support or partnership relationships with, our advisers, consultants, bankers, vendors or sub-contractors.
•	Data is information, which is stored electronically, on a computer, or in certain paper-based filing systems.
•	Data Controller is a person responsible for determining the manner in which Personal Data would be processed.
•	NDPR means the Nigerian Data Protection Regulations.
•	NITDA means the National Information Technology Development Agency.
•	Personal Data is the information relating to an identified or identifiable natural person. These include a name, gender, a photo, an email address, bank details, medical information, computer internet protocol address and any other information specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.
•	Processing is any activity that involves use of Personal Data. It includes obtaining, recording or holding the data, or carrying out any operation or set of operations on the data including organizing, amending, recording, retrieving, using, disclosing, erasing or destroying it. Processing also includes transferring personal data to third parties.
•	Sensitive Personal Data includes information about a person’s racial origin, political opinions, religious or similar beliefs, trade union membership, physical or mental health or condition or sexual life.
Privacy Policy
1. Data Collection
We may collect, use, store and transfer different kinds of Personal Data about you which we have grouped together as follows:
a.	Identity Data includes first name, last name, username or similar identifier, title, date of birth and gender.
b.	Contact Data includes residential address, email address and telephone numbers.
c.	Human Resource Data includes information on your employment history, professional and educational information submitted upon applying for employment with us.
d.	Technical Data includes internet protocol (IP) address, domain name, your login data, browser type and version, time zone setting and location, browser plug-in types and versions, operating system and platform, and other technology on the devices you use to access this website.
e.	Profile Data includes your username and password, purchases or orders made by you, your interests, preferences, feedback and survey responses.
f.	Usage Data includes information about how you use our website and services.
g.	Marketing and Communications Data includes your preferences in receiving marketing communications from us and our Affiliated Third Parties and your communication preferences.
You provide this information through direct interaction when you visit our website, sign up to our newsletters or publications, request marketing materials to be sent to you, respond to surveys, complete our feedback or comment form, provide your business card to any of our staff, sign our visitor management form, complete other forms, apply for employment through our careers page, or contact us to request for any information or other correspondence by post, email, our website or otherwise.
h.	As you interact with our website, we will automatically collect technical data about your equipment, browsing actions and patterns. We collect this data by using cookies, and other similar technologies. Please see our cookies policy for further details.
We do not intentionally or knowingly collect any Sensitive Personal Data. We ask that you do not send to us nor disclose such Sensitive Personal Data save where required for a specific purpose.

2. Your Personal Data and how it is Used
a.	Primarily, we collect, process and store your Personal Data to help us better connect with you. The following are the purposes for which we collect your Personal Data:
To monitor, review, evaluate and improve your experience when you visit our website.
b.	To analyze the traffic on our website, including determining the number of visitors to the website and analyze how they navigate the website.
c.	To invite you to complete a survey or provide feedback to us on specific matters.
d.	At any time, you request information from us via a form or other electronic transmission we may use your Personal Data to fulfill that request and keep a record of such request and how it was handled, for quality assurance and service improvement purposes.
e.	To keep you updated on our activities, programs and events where your explicit consent has been given.
f.	To notify you of changes to our websites or relevant processes.
g.	We may also use your information or allow Affiliated Third Parties such as our affiliate companies or partners use of this Personal Data, to offer you information about unrelated products or services you may be interested in. We or such Affiliated Third Parties can only communicate with you if you have expressly consented to such communication and data use.
h.	We may share your personal data with Affiliated Third Parties such as service providers who we have engaged to assist with providing certain services on our behalf, for which they require your personal data.
i.	Where we have any contracts with you which create a commitment, we may require contact or use of your information to perform the contract.
j.	To process or manage your appointments with any of our staff.
k.	To fulfill legal/ regulatory obligations or to report any criminal or unethical activity.
l.	To store either on our central computer system or a third-party Computer’s central computer system for archiving and back up purposes.
m.	Be aware that we do not reveal identifiable information about you to our advertisers, though we may at times share statistical visitor information with our advertisers.

3. Change of Purpose
We will only use your Personal Data for the aforementioned purposes, unless we reasonably consider that we need to use it for another reason and that reason is compatible with the original purpose. If you wish to get an explanation as to how the processing for the new purpose is compatible with the original purpose, please contact us. If we need to use your Personal Data for an unrelated purpose, we will notify you and request for your express consent.
4. Your Personal Data Rights
Data Protection Laws provides you with certain rights in relation to the information that we collect about you.
a.	The right to withdraw consent previously given to us or our Affiliated Third Parties. In order to make use of your personal data, we would have obtained your consent. For consent to be valid, it must be given voluntarily. In line with regulatory requirements, consent cannot be implied, and we ensure that you have the opportunity to read our data protection privacy policy before you provide your consent. Consent in respect of Sensitive Personal Data must be explicit and will be given by you in writing to us. The consent of minors (under the age of 18) will always be protected and obtained from the minor’s representatives in accordance with applicable regulatory requirements.
b.	You can ask us or Affiliated Third Parties to stop sending you marketing messages at any time by unsubscribe or unchecking relevant boxes to adjust your marketing preferences or by following the opt-out links on any marketing message sent to you.
b. The right to request that we delete your Personal Data that is in our possession, subject however to retention required for legal purposes and the time required technically to delete such information.
c.	The right to request for access to your Personal Data or object to us processing the same. Where personal data is held electronically in a structured form, such you have a right to receive that data in a common electronic format.
d.	The right to update your Personal Data that is kept with us. You may do this at anytime your personal data changes and you wish to update us.
e.	The right to receive your Personal Data and have it transferred to another Data Controller, as applicable.
f.	The right to lodge a complaint. You may exercise any of the above stated rights following our Data Subject Access Request Procedure.

5. Persons who have access to your Personal Data
In addition to our staff who have a business need to know, the following trusted third parties have access to your information:
a.	We use a customer relationship management tool to help manage our contact database and send out email communications to you.
b.	Our Affiliate Third Parties who require your information for the same purposes described in this Policy and who have adopted similar privacy policy standards further to contractual obligations to us under a third party data processing agreement we have entered with them.
c.	Professional service providers such as IT service providers and website hosts.
d.	Regulatory authorities.
We will transfer your Personal Data to only those Affiliated Third Parties who we are sure can offer the required level of protection to your privacy and information and who are also contractually obligated to us to do so. We do not and will not at any point in time sell your Personal Data. We require all Affiliated Third Parties to respect the security of your personal data and to treat it in accordance with the law. We do not allow our professional service providers to use your Personal Data for their own purposes and only permit them to process your Personal Data for specified purposes and in accordance with our instructions.
6. Security & Confidentiality
Information submitted by you is stored on secure servers we have which are encrypted and access is restricted to only authorized persons in charge of maintaining the servers. We have put in place physical, electronic and procedural processes that safeguard and protect your information against unauthorized access, modification or erasure. However, we cannot guarantee 100% security as no security program is completely fool proof.
In the unlikely event that we experience any breach to your personal data, such breach shall be handled in accordance with our Personal Data Breach Management Procedures. All such breaches shall be notified to the NITDA within 72 hours of occurrence and where deemed necessary, based on the severity and potential risks, we shall notify you of such occurrence, steps taken and remedies employed to prevent a re-occurrence.
Our staff also have an obligation to maintain the confidentiality of any Personal Data held by us.
As you know, transmission of data on the internet is never guaranteed regarding safety. It is impossible to completely guarantee your safety with electronic data and transmission. You are therefore at your own risk if you elect to transmit any data electronically.

7. Transfer of Personal Data outside Nigeria
The Personal Data we collect may be transferred to and processed in another country other than your country of residence for the purposes stated above. The data protection laws in those countries may be different from, and less stringent than the laws applicable in your country of residence.
By accepting this Policy or by providing your Personal Data to us, you expressly consent to such transfer and Processing. We will however take all reasonable steps to ensure that your data is treated securely and transfer of your Personal Data will only be done in accordance with the requirements of applicable laws and to parties who have put in place adequate controls to secure and protect your Personal Data.
8. Retention of Personal Data
We retain your Personal Data for no longer than reasonably necessary for the purposes set out in this Policy and in accordance with legal, regulatory, tax, accounting or reporting requirements.
We may retain your Personal Data for a longer period in the event of a complaint or if we reasonably believe there is a prospect of litigation in respect to our relationship with you.
To determine the appropriate retention period for personal data, we consider the amount, nature and sensitivity of the Personal Data, the potential risk of harm from unauthorized use or disclosure of your Personal Data, the purposes for which we process your Personal Data and whether we can achieve those purposes through other means, and the applicable legal, regulatory, tax, accounting or other requirements.
Where your Personal Data is contained within a document, the retention period applicable to such type of document in our document retention policy shall apply.
9. Third Party Links
This website or our email communication may include links to third party websites, plug-ins and applications. Clicking on those links or enabling those connections may allow third parties to collect or share data about you. We do not control these third-party websites and are not responsible for their privacy statements. When you leave our website, we encourage you to read the privacy policy of every website you visit.
10. Cookies Policy
a.	Our advertisers and organization may have the occasion to collect information in regard to your computer for our services. The information is gained in a statistical manner for our use or advertisers on our site.
b.	Data gathered will not identify you personally. It is strictly aggregate statistical data about our visitors and how they used our resources on the site. No identifying Personal Data will be shared at any time via cookies.
c.	Close to the above, data gathering can be about general online use through a cookie file. When used, cookies are automatically placed in your hard drive where information transferred to your computer can be found. These cookies are designed to help us correct and improve our site’s services for you.
d.	You may elect to decline all cookies via your computer or set up alerts to prompt you when websites set or access cookies. Every computer has the ability to decline file downloads like cookies. Your browser has an option to enable the declining of cookies. If you do decline cookie downloads you may be limited to certain areas of our site, as there are parts of our site that require cookies.
e.	Any of our advertisers may also have a use for cookies. We are not responsible, nor do we have control of the cookies downloaded from advertisements. They are downloaded only if you click on the advertisement.

11. Subject Access Request Response Procedure
a.	Where you wish to exercise any of your data privacy rights you shall make a formal request by completing the Subject Access Request Form (SAR Form) and sending the completed form via email to us at privacy@kcmfb.com.
b.	We shall contact you within 5 working days of the receipt of the SAR Form to confirm receipt of the subject access request and may request additional information to verify and confirm the identity of the individual making the request.
c.	On receiving any request from you, we shall record the request and carry out verification of the identity of the individual making the request using the details provided in the SAR Form and a valid means of identification such as international passport, driver’s license, national identification card or any other acceptable means of identification.
d.	Where the request is from a third party (such as relative or your representative), we will verify their authority to act for you and may contact you to confirm their identity and request your consent to disclose the information.
e.	When your identity is verified, we shall coordinate the gathering of all information collected with respect to you in a concise, transparent, intelligible and easily accessible form, using clear and plain language with a view to responding to the specific request. The information may be provided in writing, or by other means, including, where appropriate, by electronic means or orally provided that your identity is proven by other means. We may also contact you to ask you for further information in relation to your request to speed up our response.
f.	Where the information requested relates directly or indirectly to another person, we will seek the consent of that person before processing the request. However, where disclosure would adversely affect the rights and freedoms of others and we are unable to disclose the information, we will inform you promptly, with reasons for that decision.
g.	Fees and Timeframe.
h.	We shall ensure that we provide the information required by you within a period of one month from the receipt of the request. Occasionally it could take us longer than a month if your request is particularly complex or you have made a number of requests. In this case, we will notify you and keep you updated. However, where we are unable to act on your request, we shall inform you promptly at least within one month of receipt of the request of the reasons for not taking action and give you the option of lodging a complaint with the NITDA, in line with the NDPR.
i.	Where the request relates to any perceived violation of your rights, we shall take appropriate steps to remedy such violations, once confirmed. Remedies shall include but not limited to the investigation and reporting to appropriate authorities, recovering the personal data, correcting it and/ or enhancing controls around it. You shall be appropriately informed of the remedies employed.
j.	Any information provided to you by us shall be provided free of charge. However, where requests are manifestly unfounded or excessive in particular because of their repetitive or cumbersome nature, we may: Charge a reasonable fee taking into account the administrative costs of providing the information or communication, taking the action required or making a decision to refuse to act on the request; or Write a letter to you stating refusal to act on the request and copying the National Information Technology Development Agency (NITDA).

12. Exceptions to Data Subjects Access Rights
To the extent permitted by applicable laws, we may refuse to act on your request, if at least one of the following applies:
a. in compliance with a legal obligation to which we are subject;
b. protecting your vital interests or of another natural person; and
c. For public interest or in exercise of official public mandate vested in us.

Contacting Us
We welcome any queries, requests you may have regarding our Data Protection Privacy Policies, or our privacy practices. Please feel free to contact us at info@kcmfb.com or by completing the Subject Access Request Form (SAR Form).

